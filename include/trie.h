#ifndef TRIE_H
#define TRIE_H

#include <stdbool.h>

#define ALPHABET "abcdefghijklmnopqrstuvwxyz"
#define ALPHABET_LENGTH 26

struct trie_node {
	char *value;
	struct trie_node *father; // needed for removing words
	int children_count;
	struct trie_node *children[ALPHABET_LENGTH];
};
typedef struct trie_node trie_node;

struct trie {
	trie_node *root;
};
typedef struct trie trie;

struct subtrie {
	char *prefix;
	trie_node *root;
};
typedef struct subtrie subtrie;

trie *new_trie(void);
void destroy_trie(trie *t);

bool trie_add_word(trie *, const char *);
bool trie_remove_word(trie *, const char *);
bool trie_contains(trie *, const char *);
char *trie_search(trie *, const char *);

subtrie *trie_search_subtrie(trie *, const char *);
bool subtrie_has_child(subtrie *, char);
bool subtrie_contains(subtrie *, const char *);

const char *valid_symbols(void);

#ifdef TESTING
void debug_print(trie *t);

trie_node *new_node(void);
void free_node(trie_node *node);
void destroy_trie_node(trie_node *node);

subtrie *new_subtrie(const char *prefix, trie_node *root);
void destroy_subtrie(subtrie *st);

bool is_valid(const char *word);
#endif

#endif
