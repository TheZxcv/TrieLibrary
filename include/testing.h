#ifndef TESTING_H
#define TESTING_H

#define TESTING
#include "trie.h"

void test(bool actual, bool expected, const char *format, ...);

#endif
