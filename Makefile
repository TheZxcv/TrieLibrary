CC=gcc
CFLAGS=-c -ggdb -Wall -O -Iinclude/
#CFLAGS=-c -Wall -O3 -Iinclude/ -s
LDFLAGS=-L . -ltrie -static
SRC_DIR=source
TEST_DIR=test
BUILD_DIR=build
SOURCES=$(wildcard $(SRC_DIR)/*.c)
OBJECTS=$(notdir $(SOURCES))
OBJECTS:=$(OBJECTS:%.c=$(BUILD_DIR)/%.o)
AR=ar
ARFLAGS=-r -s
LIB=libtrie.a
LIB_SHARED=$(LIB:%.a=%.so)

all: $(SOURCES) $(LIB) $(LIB_SHARED)

.PHONY: static
static: $(LIB)
$(LIB): $(OBJECTS)
	$(AR) $(ARFLAGS) $(LIB) $(OBJECTS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -fPIC -o $@ $^

.PHONY: shared
shared: $(LIB_SHARED)
$(LIB_SHARED): $(OBJECTS)
	$(CC) -shared $(OBJECTS) -o $(LIB_SHARED)

.PHONY: test
test: $(LIB)
	$(CC) $(CFLAGS) $(TEST_DIR)/$@.c -o $(BUILD_DIR)/$@.o
	$(CC) $(CFLAGS) $(TEST_DIR)/testing.c -o $(BUILD_DIR)/testing.o
	$(CC) $(BUILD_DIR)/testing.o $(BUILD_DIR)/$@.o -o $(TEST_DIR)/$@ $(LDFLAGS) 
	./$(TEST_DIR)/$@

.PHONY: clean
clean:
	rm -rf $(LIB) $(LIB_SHARED) $(OBJECTS)

