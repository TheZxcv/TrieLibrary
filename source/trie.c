#include "trie.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>

#define CHAR2INDEX(c) (tolower((c)) - 'a')
#define INDEX2CHAR(i) ('a' + (i))

trie_node *new_node(void) {
	trie_node *n = calloc(1, sizeof(trie_node));
	assert(n != NULL);
	return n;
}

void free_node(trie_node *node) {
	free(node->value);
	free(node);
}

void destroy_trie_node(trie_node *node) {
	int i;
	for(i = 0; i < ALPHABET_LENGTH; i++)
		if(node->children[i])
			destroy_trie_node(node->children[i]);
	free_node(node);
}

void destroy_trie(trie *t) {
	destroy_trie_node(t->root);
	free(t);
}

trie *new_trie(void) {
	trie *t = malloc(sizeof(trie));
	assert(t != NULL);
	t->root = new_node();
	return t;
}

void destroy_subtrie(subtrie *st) {
	free(st->prefix);
	free(st);
}

subtrie *new_subtrie(const char *prefix, trie_node *root) {
	subtrie *st = malloc(sizeof(subtrie));
	assert(st != NULL);
	st->root = root;

	size_t len = strlen(prefix);
	st->prefix = malloc(sizeof(char) * (len + 1));
	assert(st->prefix != NULL);
	strcpy(st->prefix, prefix);

	return st;
}

bool is_valid(const char *word) {
	// empty word
	if(*word == '\0')
		return false;

	for(; *word; word++) {
		char c = tolower(*word);
		if('a' > c || c > 'z')
			return false;
	}
	return true;
}

bool trie_add_word(trie *t, const char *word) {
	if(!is_valid(word))
		return false;

	trie_node *curr_node = t->root;
	int i = 0;
	while(word[i]) {
		int index = CHAR2INDEX(word[i]);
		assert(index >= 0 && index < ALPHABET_LENGTH);

		if(curr_node->children[index] == NULL) {
			curr_node->children[index] = new_node();
			curr_node->children[index]->father = curr_node;
			curr_node->children_count++;
		}

		curr_node = curr_node->children[index];
		i++;
	}

	int len = strlen(word);
	curr_node->value = malloc(sizeof(char) * (len+1));
	assert(curr_node->value != NULL);
	strcpy(curr_node->value, word);

	return true;
}

trie_node *trie_search_node(trie_node *root, const char *key) {
	if(!is_valid(key))
		return NULL;

	trie_node *curr_node = root;
	while(*key) {
		int index = CHAR2INDEX(*key);
		if(curr_node->children[index] == NULL)
			return NULL;
		curr_node = curr_node->children[index];
		key++;
	}

	return curr_node;
}

bool trie_remove_word(trie *t, const char *word) {
	trie_node *last = trie_search_node(t->root, word);
	if(last == NULL)
		return false;

	int curr_key = strlen(word) - 1;
	last->value = NULL;
	
	while(last->children_count == 0) {
		trie_node *father = last->father;
		if(father == NULL)
			// let's not remove the root node
			break;

		free_node(last);

		int index = CHAR2INDEX(word[curr_key]);
		father->children[index] = NULL;
		father->children_count--;

		last = father;
		curr_key--;
	}

	return true;
}

char *trie_search(trie *t, const char *key) {
	trie_node *node = trie_search_node(t->root, key);
	return (node) ? node->value : NULL;
}

bool trie_contains(trie *t, const char *key) {
	return trie_search(t, key) != NULL;
}

subtrie *trie_search_subtrie(trie *t, const char *prefix) {
	trie_node *subtrie_root;

	subtrie_root = trie_search_node(t->root, prefix);
	if(subtrie_root == NULL)
		return NULL;

	return new_subtrie(prefix, subtrie_root);
}

bool subtrie_has_child(subtrie *st, char key) {
	int index = CHAR2INDEX(tolower(key));
	assert(index >= 0 && index < ALPHABET_LENGTH);
	return st->root->children[index] != NULL;
}

bool subtrie_contains(subtrie *st, const char *key) {
	trie_node *node = trie_search_node(st->root, key);
	return node != NULL;
}

const char *valid_symbols(void) {
	return ALPHABET;
}

void debug_print(trie *t) {
	printf("not implemented yet.\n");
}
