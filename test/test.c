#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "testing.h"

void test_filter(const char *w, bool expected) {
	bool actual = is_valid(w);
	test(actual, expected, "filter: %s, %s\n", w, expected ? "true" : "false");
}

void test_add(trie *t, const char *w, bool expected) {
	trie_add_word(t, w);
	bool act_contains = trie_contains(t, w);
	bool act_strcmp = strcmp(trie_search(t, w), w) == 0;

	test(act_contains && act_strcmp, expected, "add: %s\n", w);

	assert(act_contains == expected);
	assert(act_strcmp == expected);
}

void test_search(trie *t, const char *w, bool expected) {
	bool actual = trie_contains(t, w);
	test(actual, expected, "search: %s, %s\n", w, expected ? "true" : "false");
}

void test_remove(trie *t, const char *w, bool expected) {
	bool actual = trie_remove_word(t, w);
	test(actual, expected, "remove: %s, %s\n", w, expected ? "true" : "false");
}

void test_search_subtrie(trie *t, const char *key, bool expected) {
	subtrie *st = trie_search_subtrie(t, key);
	bool act_search = st != NULL;
	bool act_keycmp = false;
	if(act_search) {
		act_keycmp = strcmp(st->prefix, key) == 0;
		destroy_subtrie(st);
	}

	test(act_search && act_keycmp, expected, "search_subtrie: %s\n", key);

	assert(act_search == expected);
	assert(act_keycmp == expected);
}

void test_haschild(subtrie *st, char key, bool expected) {
	bool actual = subtrie_has_child(st, key);
	test(actual, expected, "has_child: %c, %s\n", key, expected ? "true" : "false");
}

void test_contains(subtrie *st, const char *key, bool expected) {
	bool actual = subtrie_contains(st, key);
	test(actual, expected, "contains: %s, %s\n", key, expected ? "true" : "false");
}

void trie_test(void) {
	test_filter("testing", true);
	test_filter("1nval1d", false);
	test_filter("", false);
	test_filter("DiffenrEntCase", true);

	trie *t = new_trie();
	test_add(t, "word", true);
	test_add(t, "alice", true);
	test_add(t, "asap", true);
	test_add(t, "alter", true);

	test_search(t, "alice", true);
	test_search(t, "notfound", false);

	test_remove(t, "asap", true);
	test_search(t, "asap", false);
	test_remove(t, "asap", false);

	test_remove(t, "word", true);
	test_remove(t, "alice", true);
	test_remove(t, "alter", true);
	assert(t->root->children_count == 0);
	int i;
	for(i = 0; i < ALPHABET_LENGTH; i++)
		assert(t->root->children[i] == NULL);

	test_add(t, "word", true);
	test_add(t, "alice", true);
	test_add(t, "asap", true);
	test_add(t, "alter", true);

	printf("Testing subtrie:\n");

	test_search_subtrie(t, "al", true);
	test_search_subtrie(t, "bl", false);

	subtrie *st;
	st = trie_search_subtrie(t, "al");

	test_haschild(st, 't', true);
	test_haschild(st, 'i', true);
	test_haschild(st, 'v', false);

	test_contains(st, "ice", true);
	test_contains(st, "ter", true);
	test_contains(st, "alice", false);
	test_contains(st, "fail", false);

	destroy_trie(t);
	
	printf("\x1b[1m---> ALL TESTS PASSED! <---\x1b[0m\n");
}

int main(void) {
	trie_test();
	return EXIT_SUCCESS;
}
