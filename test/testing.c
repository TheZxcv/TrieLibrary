#include "testing.h"

#include <stdio.h>
#include <assert.h>
#include <stdarg.h>

void test(bool actual, bool expected, const char *format, ...) {
	if(actual == expected)
		printf("\x1b[32mPASSED!\x1b[0m\t");
	else
		printf("\x1b[1;31mFAILED!\x1b[0m\t");

	va_list ap;
	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
	
	assert(actual == expected);
}
